<?php
if ( $properties ) {
	?>
	<?php if ( wp_is_mobile() ) { ?>
		<div class="o-layout-6__mobile">
			<ul>
				<?php foreach ( $properties as $property ) { ?>
					<li>
						<table>
							<tr>
								<th><?php _e( 'Reference' ); ?></th>
								<td><?php echo $property->post_title; ?></td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_block' ); ?>>
								<th><?php _e( 'Block' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_block', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_portal' ); ?>>
								<th><?php _e( 'Portal' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_portal', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_floor' ); ?>>
								<th><?php _e( 'Floor' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_floor', $property->ID );
									echo $field['value'];
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_plot_size' ); ?>>
								<th><?php _e( 'Plot Size (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_plot_size', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_built_size' ); ?>>
								<th><?php _e( 'Built Size (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_built_size', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_internal_surface' ); ?>>
								<th><?php _e( 'Internal Surface (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_internal_surface', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_bedrooms' ); ?>>
								<th><?php _e( 'Bedrooms' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_bedrooms', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_bathrooms' ); ?>>
								<th><?php _e( 'Bathrooms' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_bathrooms', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_terrace' ); ?>>
								<th><?php _e( 'Terrace (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_terrace', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_covered_terrace' ); ?>>
								<th><?php _e( 'Covered terrace (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_covered_terrace', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_garden' ); ?>>
								<th><?php _e( 'Garden (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_garden', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_pool' ); ?>>
								<th><?php _e( 'Pool (m²)' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_pool', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_garage' ); ?>>
								<th><?php _e( 'Garage' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_garage', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_parking' ); ?>>
								<th><?php _e( 'Parking' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_specification_parking', $property->ID );
									if ( $field['value'] ) {
										echo $field['value'];
									} else {
										echo '-';
									}
									?>
								</td>
							</tr>
							<tr>
								<th><?php _e( 'Status' ); ?></th>
								<td>
									<?php
									$field = get_field_object( 'pws_property_status_status', $property->ID );
									//var_dump( $field );
									$value = get_field( 'pws_property_status_status', $property->ID );
									if ( $field && '' != $value ) {
										$label = $field['choices'][ $value ];
										echo $label;
									}
									?>
								</td>
							</tr>
							<tr>
								<th><?php _e( 'Plan' ); ?></th>
								<td>
									<?php
									$field = get_field( 'pws_property_specification_property_plans', $property->ID );
									if ( $field['url'] ) {
										?>
										<a href="<?php echo $field['url']; ?>" target="_blank" class="o-button o-button--small"><?php _e( 'Plan', 'pws-investinspain' ); ?></a>
									<?php } ?>
								</td>
							</tr>
						</table>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } else { ?>
		<table class="o-layout-6__table">
			<thead>
				<tr>
					<th><?php _e( 'Reference' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_block' ); ?>><?php _e( 'Block' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_portal' ); ?>><?php _e( 'Portal' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_floor' ); ?>><?php _e( 'Floor' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_plot_size' ); ?>><?php _e( 'Plot Size (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_built_size' ); ?>><?php _e( 'Built Size (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_internal_surface' ); ?>><?php _e( 'Internal Surface (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_bedrooms' ); ?>><?php _e( 'Bedrooms' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_bathrooms' ); ?>><?php _e( 'Bathrooms' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_terrace' ); ?>><?php _e( 'Terrace (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_covered_terrace' ); ?>><?php _e( 'Covered terrace (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_uncovered_terrace' ); ?>><?php _e( 'Uncovered terrace (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_garden' ); ?>><?php _e( 'Garden (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_pool' ); ?>><?php _e( 'Pool (m²)' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_garage' ); ?>><?php _e( 'Garage' ); ?></th>
					<th <?php Pws_Investinspain\Property::get_show_property_specification( 'pws_property_specification_parking' ); ?>><?php _e( 'Parking' ); ?></th>
					<th><?php _e( 'Status' ); ?></th>
					<th><?php _e( 'Plan' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ( $properties as $property ) {
					?>
					<tr>
						<td><?php echo $property->post_title; ?></td>
						<?php
						$field = get_field_object( 'pws_property_specification_block', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_portal', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_floor', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							echo $field['value'];
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_plot_size', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_built_size', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_internal_surface', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_bedrooms', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_bathrooms', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_terrace', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_covered_terrace', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_uncovered_terrace', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_garden', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_pool', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_garage', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<?php
						$field = get_field_object( 'pws_property_specification_parking', $property->ID );
						?>
						<td <?php Pws_Investinspain\Property::get_show_property_specification( $field['key'] ); ?>> 
							<?php
							if ( $field['value'] ) {
								echo $field['value'];
							} else {
								echo '-';
							}
							?>
						</td>
						<td>
							<?php
							$field = get_field_object( 'pws_property_status_status', $property->ID );
							//var_dump( $field );
							$value = get_field( 'pws_property_status_status', $property->ID );
							if ( $field && '' != $value ) {
								$label = $field['choices'][ $value ];
								echo $label;
							}
							?>
						</td>
						<td>
							<?php
							$field = get_field( 'pws_property_specification_property_plans', $property->ID );
							if ( $field['url'] ) {
								?>
								<a href="<?php echo $field['url']; ?>" target="_blank" class="o-button o-button--small"><?php _e( 'Plan', 'pws-investinspain' ); ?></a>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php } ?>
<?php } ?>
