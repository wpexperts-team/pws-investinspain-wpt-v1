<?php

$home_id       = Theme_Admin::get_page_template_id_by_name( 'page-home' );
$project_id    = Theme_Admin::get_page_template_id_by_name( 'page-project' );
$location_id   = Theme_Admin::get_page_template_id_by_name( 'page-location' );
$properties_id = Theme_Admin::get_page_template_id_by_name( 'page-properties' );
$gallery_id    = Theme_Admin::get_page_template_id_by_name( 'page-gallery' );

// Home
$home1 = new Block_Component_1( $home_id, 'home1', 'Highlights', 1 );
$home2 = new Block_Component_3( $home_id, 'home2', 'Project Intro', 2 );
$home3 = new Block_Component_3( $home_id, 'home3', 'Location', 3 );

// Project
$project1 = new Block_Component_2( $project_id, 'project1', 'Text Block', 1 );
$project2 = new Block_Component_3( $project_id, 'project2', 'Text Block 2', 2 );
$project3 = new Block_Component_4( $project_id, 'project4', 'Gallery', 3 );

// Location
$location1 = new Block_Component_5( $location_id, 'location1', 'Image', 1 );

// Gallery
$gallery1 = new Block_Component_4( $gallery_id, 'gallery1', 'Gallery', 1 );
