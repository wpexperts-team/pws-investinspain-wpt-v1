<?php
namespace WP_Experts;

if ( class_exists( 'woocommerce' ) ) {
}

class woocommerce {

  public function __construct() {
    add_action( 'after_setup_theme', array( $this, 'woocommerce_support' ) );
    add_filter( 'woocommerce_enqueue_styles', array( $this, 'disable_default_stylesheet' ) );
    add_filter( 'woocommerce_register_post_type_product', array( $this, 'add_product_revisions' ) );
    //add_action('woocommerce_before_main_content', array( $this, 'woocommerce_before_main_content' ) );
    //add_action('woocommerce_after_main_content', array( $this, 'woocommerce_after_main_content' ) );
    add_filter( 'loop_shop_columns', array( $this, 'loop_columns' ) );
    add_action( 'widgets_init', array( $this, 'register_woocommerce_sidebar' ) );
  }

  public function woocommerce_support() {
    add_theme_support( 'woocommerce' );
  }

  public function disable_default_stylesheet() {
    return;
  }

  public function add_product_revisions( $args ) {
    $args['supports'][] = 'revisions';
    return $args;
  }

  function woocommerce_before_main_content() {
    echo '<section class="o-section">';
      echo '<div class="c-row">';
        echo '<div class="c-row__container">';
  }

  function woocommerce_after_main_content() {
        echo '</div>';
      echo '</div>';
    echo '</section>';
  }

  public function loop_columns() {
    if ( ! function_exists( 'loop_columns' ) ) {
      return 3;
    }
  }

  public function register_woocommerce_sidebar() {
    register_sidebar( array(
      'name' => __( 'WooCommerce Sidebar', 'theme-boilerplate' ),
      'id' => 'woocommerce-sidebar-1',
      'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-boilerplate' ),
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget'  => '</div>',
    	'before_title'  => '<h3 class="widget-title">',
    	'after_title'   => '</h3>',
    ));
  }
}

new woocommerce();
