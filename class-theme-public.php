<?php

class Theme_Public {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_google_fonts' ) );
		add_filter( 'script_loader_src', array( $this, 'remove_script_version' ), 15, 1 );
		add_filter( 'style_loader_src', array( $this, 'remove_script_version' ), 15, 1 );
		add_filter( 'get_the_archive_title', array( $this, 'get_the_archive_title' ) );
	}

	public function enqueue_styles() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'theme-styles', get_template_directory_uri() . '/css/styles.css' );
		wp_register_style( 'fontawesome', '//use.fontawesome.com/releases/v5.3.1/css/all.css' );
		wp_enqueue_style( 'fontawesome' );
	}

	public function enqueue_scripts() {
	}

	public function load_google_fonts() {
		$query_args = array(
			'family' => 'Lato:300,400,700,900|Oswald:300,400,500,600,700',
			'subset' => 'latin,latin-ext',
		);
		wp_register_style( 'google-fonts', add_query_arg( $query_args, '//fonts.googleapis.com/css' ) );
		wp_enqueue_style( 'google-fonts' );
	}

	// TODO: Move this to a theme default settings plugin
	public function get_the_archive_title( $title ) {
		if ( is_category() ) {
			$title = single_cat_title( '', false );
		} elseif ( is_tag() ) {
			$title = single_tag_title( '', false );
		} elseif ( is_author() ) {
			$title = get_the_author();
		}
		return $title;
	}

	// TODO: Move this to a theme default settings plugin
	public function remove_script_version( $src ) {
		//var_dump( 'Old: ' . $src );
		if ( strpos( $src, '?ver=' ) ) {
			//var_dump( 'Has ?ver' );
			$src = remove_query_arg( 'ver', $src );
			//var_dump( 'New: ' . $src );
			if ( strpos( $src, 'googleapis' ) === false ) {
				//$parts = explode( '?', $src );
				//var_dump( 'Normal' );
				//var_dump( $parts[0] );
				//return $parts[0];
			} else {
			}
		} else {
		}
		//var_dump( 'Final: ' . $src );
		return $src;
	}
}
