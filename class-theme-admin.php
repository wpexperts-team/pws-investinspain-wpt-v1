<?php

class Theme_Admin {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		$this->add_image_sizes();
		add_filter( 'image_size_names_choose', array( $this, 'set_custom_image_sizes' ) );
		add_filter( 'upload_mimes', array( $this, 'set_mime_types' ) );
		add_action( 'after_setup_theme', array( $this, 'add_theme_support' ) );
		add_action( 'after_setup_theme', array( $this, 'register_nav_menus' ) );
		require_once 'classes/class-register-sidebar.php';
		require_once 'classes/class-block-component.php';
		$this->set_term_description_shortcodes();
		$this->set_excerpt_shortcodes();
		$this->set_comments_shortcodes();
		$this->add_post_meta();
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/admin/css/styles.css' );
	}

	public function enqueue_scripts() {
	}

	public function add_theme_support() {
		add_theme_support( 'html5' );
	}

	public function add_image_sizes() {
		add_image_size( 'medium_square', 600, 600, true );
		add_image_size( 'xl', 1750 );
		add_image_size( 'xxl', 2400 );
	}

	public function set_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function set_custom_image_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'medium_square' => __( 'Medium Square', 'pws-investinspain' ),
				'xl'            => __( 'XL', 'pws-investinspain' ),
				'xxl'           => __( 'XXL', 'pws-investinspain' ),
			)
		);
	}

	// Excerpts
	public function set_excerpt_shortcodes() {
		add_filter( 'the_excerpt', 'shortcode_unautop' );
		add_filter( 'the_excerpt', 'do_shortcode' );
	}

	// Category, Tag, and Taxonomy Descriptions
	public function set_term_description_shortcodes() {
		add_filter( 'term_description', 'shortcode_unautop' );
		add_filter( 'term_description', 'do_shortcode' );
	}

	// Comments
	public function set_comments_shortcodes() {
		add_filter( 'comment_text', 'shortcode_unautop' );
		add_filter( 'comment_text', 'do_shortcode' );
	}

	public function register_nav_menus() {
		register_nav_menus(
			array(
				'primary'   => __( 'Primary', 'pws-investinspain' ),
				'secondary' => __( 'Secondary', 'pws-investinspain' ),
				'legal'     => __( 'Legal', 'pws-investinspain' ),
			)
		);
	}

	public function add_post_meta() {
		$file = get_template_directory() . '/data/post-meta.php';
		include $file;
	}

	static public function get_page_template_id_by_name( $name = null ) {
		$pages = get_pages(
			array(
				'meta_key'   => '_wp_page_template',
				'meta_value' => 'page-templates/' . $name . '.php',
			)
		);
		if ( $pages ) {
			return $pages[0]->ID;
		}
	}

	public static function save_component( $post_id = null, $meta_name = null, $data = null ) {
		update_post_meta( $post_id, get_the_ID() . $meta_name, $data );
	}
}
