<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PWS_INVESTINSPAIN_VERSION', '1.0.0' );

if ( ! class_exists( 'Pws_Investinspain_theme' ) ) {

	class Pws_Investinspain_Theme {

		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
			$this->theme_admin();
			$this->theme_public();
			require_once 'plugins/gravityforms/gravityforms.php';
			//require_once 'plugins/wordpress-seo/wordpress-seo.php';
			//require_once 'plugins/wpml/wpml.php';
			//require_once 'wp-classes/class-wp-essentials.php';
		}

		// TODO: Define some constants we can use throughout the theme.
		public function initialize() {

		}

		public function theme_setup() {
			load_theme_textdomain( 'pws-investinspain', get_template_directory() . '/languages' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'menus' );
			add_theme_support( 'widgets' );
			add_theme_support( 'title-tag' );
			add_post_type_support( 'page', 'excerpt' );
			// TODO: Fix underscores issue
			$GLOBALS['content_width'] = apply_filters( 'test_underscores_content_width', 640 );
		}

		public function theme_admin() {
			require_once 'class-theme-admin.php';
			$admin = new Theme_Admin();
		}

		public function theme_public() {
			require_once 'class-theme-public.php';
			$public = new Theme_Public();
		}
	}
}

new Pws_Investinspain_Theme();
