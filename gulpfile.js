"use strict";

// Project variables
var siteUrl = "http://pws-investinspain.develop";
var themeName = "pws-investinspain";
// Gulp packages
var gulp = require("gulp");
var sass = require("gulp-sass");
var cssnano = require("gulp-cssnano");
var sourcemaps = require("gulp-sourcemaps");
var notify = require("gulp-notify");
var zip = require("gulp-zip");
// Browsersync
var browserSync = require("browser-sync").create();
// Post CSS
var postcss = require("gulp-postcss");
//var postCSSOptions = require('./config.postcss.json');
// Autoprefixer
var autoprefixer = require("gulp-autoprefixer");
//var autoprefixerOptions = postCSSOptions.autoprefixer;

// Gulp Sass
gulp.task("sass", function() {
  return (
    gulp
      .src("./sass/**/*.scss")
      .pipe(sourcemaps.init())
      .pipe(
        sass({
          outputStyle: "compressed"
        }).on("error", sass.logError)
      )
      .pipe(
        autoprefixer({
          use: ["autoprefixer"],
          autoprefixer: {
            browsers: ["last 2 versions"]
          }
        })
      )
      //.pipe(cssnano())
      .pipe(sourcemaps.write(""))
      .pipe(gulp.dest("./css/"))
      .pipe(browserSync.stream())
      .pipe(
        notify({
          message: "Sass Task on " + themeName + " completed",
          onLast: true
        })
      )
  );
});

// Gulp Sass Admin
gulp.task("sassAdmin", function() {
  return (
    gulp
      .src("./admin/sass/**/*.scss")
      .pipe(sourcemaps.init())
      .pipe(
        sass({
          outputStyle: "compressed"
        }).on("error", sass.logError)
      )
      .pipe(
        autoprefixer({
          use: ["autoprefixer"],
          autoprefixer: {
            browsers: ["last 2 versions"]
          }
        })
      )
      //.pipe(cssnano())
      .pipe(sourcemaps.write(""))
      .pipe(gulp.dest("./admin/css/"))
      .pipe(browserSync.stream())
      .pipe(
        notify({
          message: "Sass Admin Task on " + themeName + " completed",
          onLast: true
        })
      )
  );
});

// Gulp watch task
gulp.task("watch", function() {
  gulp.watch("./admin/sass/**/*.scss", gulp.series("sassAdmin"));
  gulp.watch("./sass/**/*.scss", gulp.series("sass"));
  //gulp.watch('/Users/filip/Sites/wordpress.dev/master/wp-content/themes/theme-boilerplate/sass/**/*.scss', gulp.series('sass'));
  gulp.watch("/Users/filip/WordPress/**/*.scss", gulp.series("sass"));
});

gulp.task("browser-sync", function() {
  browserSync.init({
    files: ["**/*.css", "**/*.php", "**/*.{png,jpg,gif}"],
    browser: "default",
    injectChanges: true,
    online: true,
    open: false,
    //port: 3000,
    proxy: {
      target: siteUrl
    },
    notify: true,
    watchTask: true
  });
  gulp.watch("./sass/**/*.scss", gulp.series("browser-sync"));
  //gulp.watch('/Users/filip/Sites/wordpress.dev/master/wp-content/themes/theme-boilerplate/sass/**/*.scss', gulp.series('sass'));
  gulp.watch("/Users/filip/WordPress/**/*.scss", gulp.series("browser-sync"));
});
// Gulp ZIP task
gulp.task("zip", function() {
  gulp
    .src([
      // Include
      "**/*",
      // Exclude
      "!**/*.scss",
      "!bower.json",
      "!bs-config.js",
      "!composer.json",
      "!composer.lock",
      "!config.postcss.json",
      "!gulpfile.js",
      "!package-lock.json",
      "!package.json",
      // Plugins
      "!plugins/**/sass/**/*",
      "!plugins/**/sass",
      // Node modules
      "!node_modules/**/*",
      "!node_modules",
      // Sass
      "!sass/**/*",
      "!sass",
      // Admin
      "!./admin/sass/**/*",
      "!./admin/sass"
    ])
    .pipe(zip(themeName + ".zip"))
    .pipe(gulp.dest("../"))
    .pipe(
      notify({
        message:
          themeName + " was successfully zipped to " + themeName + ".zip",
        onLast: true
      })
    );
});
// Gulp default task
gulp.task("default", function() {
  // place code for your default task here
});
