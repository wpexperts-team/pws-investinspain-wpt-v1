<?php

namespace Theme_Public\Register_Sidebar;

class Register_Sidebar {

  public function __construct() {
    add_action( 'widgets_init', array( $this, 'register_sidebar' ) );
  }

  public function register_sidebar() {
    $sidebars = array(
      array(
        'name' => __( 'Page Sidebar', 'theme-boilerplate' ),
        'id' => 'page-sidebar',
        'description' => __( 'These widgets will show on pages.' , 'theme-boilerplate' ),
        'before_widget' => '<li id="%1$s" class="wp-sidebar__widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
      ),
      array(
        'name' => __( 'Category Sidebar', 'theme-boilerplate' ),
        'id' => 'category-sidebar',
        'description' => __( 'These widgets will show on category pages.', 'theme-boilerplate' ),
        'before_widget' => '<li id="%1$s" class="wp-sidebar__widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
      ),
      array(
        'name' => __( 'Default Sidebar', 'theme-boilerplate' ),
        'id' => 'default-sidebar',
        'description' => __( 'These widgets will show on every page.' , 'theme-boilerplate' ),
        'before_widget' => '<li id="%1$s" class="wp-sidebar__widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
      )
    );
    // FIXME: the wp_parse_args does not work yet. Fix this!
    // $default_sidebar = array(
    //   'name' => __( 'Default Sidebar', 'theme-boilerplate' ),
    //   'id' => 'default-sidebar',
    //   'description' => __( 'These widgets will show on every page.' , 'theme-boilerplate' ),
    //   'before_widget' => '<li id="%1$s" class="wp-sidebar__widget %2$s">',
    //   'after_widget'  => '</li>',
    //   'before_title'  => '<h3 class="widget-title">',
    //   'after_title'   => '</h3>',
    // );
    foreach( $sidebars as $sidebar ) {
      //$args = wp_parse_args( $sidebars, $default_sidebar );
      register_sidebar( $sidebar );
    }
  }
}

new Register_Sidebar();
