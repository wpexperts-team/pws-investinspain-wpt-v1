<?php

// 1. Define ACF metabox with fields
// 2. Preview

abstract class Block_Component {

	public $post_id;
	public $key;
	public $name;
	public $menu_order;

	public function __construct( $post_id, $key, $name, $menu_order = 0 ) {
		$this->post_id    = $post_id;
		$this->key        = $key;
		$this->name       = $name;
		$this->prefix     = 'cb_' . $key . '_';
		$this->menu_order = $menu_order;
		add_action( 'init', array( $this, 'acf_data' ) );
	}

	abstract public function acf_data();
	abstract public function block_preview();
	abstract public function output();
}

class Block_Component_1 extends Block_Component {

	public function acf_data() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_' . substr( $this->prefix, 0, -1 ),
					'title'                 => $this->name,
					'fields'                => array(
						array(
							'key'               => $this->prefix . 'list_item',
							'label'             => __( 'List', 'pws-investinspain' ),
							'name'              => $this->prefix . 'list_item',
							'type'              => 'repeater',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'collapsed'         => '',
							'min'               => '',
							'max'               => '',
							'layout'            => 'row',
							'button_label'      => __( 'Add', 'pws-investinspain' ),
							'sub_fields'        => array(
								array(
									'key'               => $this->prefix . 'list_item_icon',
									'label'             => __( 'Icon', 'pws-investinspain' ),
									'name'              => $this->prefix . 'list_item_icon',
									'type'              => 'url',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'default_value'     => '',
									'placeholder'       => '',
								),
								array(
									'key'               => $this->prefix . 'list_item_title',
									'label'             => __( 'Title', 'pws-investinspain' ),
									'name'              => $this->prefix . 'list_item_title',
									'type'              => 'text',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'default_value'     => '',
									'placeholder'       => '',
									'prepend'           => '',
									'append'            => '',
									'maxlength'         => '',
								),
								array(
									'key'               => $this->prefix . 'list_item_subtitle',
									'label'             => __( 'Subtitle', 'pws-investinspain' ),
									'name'              => $this->prefix . 'list_item_subtitle',
									'type'              => 'text',
									'instructions'      => '',
									'required'          => 0,
									'conditional_logic' => 0,
									'wrapper'           => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'default_value'     => '',
									'placeholder'       => '',
									'prepend'           => '',
									'append'            => '',
									'maxlength'         => '',
								),
							),
						),

					),
					'location'              => array(
						array(
							array(
								'param'    => 'post',
								'operator' => '==',
								'value'    => $this->post_id,
							),
						),
					),
					'menu_order'            => $this->menu_order,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'left',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => 1,
					'description'           => '',
				)
			);
		endif;
	}

	public function block_preview() {}

	public function output() {}
}

class Block_Component_2 extends Block_Component {

	public function acf_data() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_' . substr( $this->prefix, 0, -1 ),
					'title'                 => $this->name,
					'fields'                => array(
						array(
							'key'               => $this->prefix . 'title',
							'label'             => __( 'Title', 'pws-investinspain' ),
							'name'              => $this->prefix . 'title',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => $this->prefix . 'content',
							'label'             => __( 'Content', 'pws-investinspain' ),
							'name'              => $this->prefix . 'content',
							'type'              => 'wysiwyg',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'tabs'              => 'all',
							'toolbar'           => 'full',
							'media_upload'      => 1,
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post',
								'operator' => '==',
								'value'    => $this->post_id,
							),
						),
					),
					'menu_order'            => $this->menu_order,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'left',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => 1,
					'description'           => '',
				)
			);
		endif;
	}

	public function block_preview() {}

	public function output() {}
}

class Block_Component_3 extends Block_Component {

	public function acf_data() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_' . substr( $this->prefix, 0, -1 ),
					'title'                 => $this->name,
					'fields'                => array(
						array(
							'key'               => $this->prefix . 'title',
							'label'             => __( 'Title', 'pws-investinspain' ),
							'name'              => $this->prefix . 'title',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => $this->prefix . 'content',
							'label'             => __( 'Content', 'pws-investinspain' ),
							'name'              => $this->prefix . 'content',
							'type'              => 'wysiwyg',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'tabs'              => 'all',
							'toolbar'           => 'full',
							'media_upload'      => 1,
						),
						array(
							'key'               => $this->prefix . 'image',
							'label'             => __( 'Image', 'pws-investinspain' ),
							'name'              => $this->prefix . 'image',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'thumbnail',
							'library'           => 'all',
							'min_width'         => '',
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => 'jpg,jpeg,png,svg',
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post',
								'operator' => '==',
								'value'    => $this->post_id,
							),
						),
					),
					'menu_order'            => $this->menu_order,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'left',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => 1,
					'description'           => '',
				)
			);
		endif;
	}

	public function block_preview() {}

	public function output() {}
}

class Block_Component_4 extends Block_Component {

	public function acf_data() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_' . substr( $this->prefix, 0, -1 ),
					'title'                 => $this->name,
					'fields'                => array(
						array(
							'key'               => $this->prefix . 'gallery',
							'label'             => __( 'Gallery', 'pws-investinspain' ),
							'name'              => $this->prefix . 'gallery',
							'type'              => 'gallery',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'min'               => '',
							'max'               => '',
							'insert'            => 'append',
							'library'           => 'all',
							'min_width'         => '',
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post',
								'operator' => '==',
								'value'    => $this->post_id,
							),
						),
					),
					'menu_order'            => $this->menu_order,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'left',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => 1,
					'description'           => '',
				)
			);
		endif;
	}

	public function block_preview() {}

	public function output() {}
}

class Block_Component_5 extends Block_Component {

	public function acf_data() {
		if ( function_exists( 'acf_add_local_field_group' ) ) :
			acf_add_local_field_group(
				array(
					'key'                   => 'group_' . substr( $this->prefix, 0, -1 ),
					'title'                 => $this->name,
					'fields'                => array(
						array(
							'key'               => $this->prefix . 'image',
							'label'             => __( 'Image', 'pws-investinspain' ),
							'name'              => $this->prefix . 'image',
							'type'              => 'image',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'array',
							'preview_size'      => 'large',
							'library'           => 'all',
							'min_width'         => '',
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => 'jpg,jpeg,png,svg',
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post',
								'operator' => '==',
								'value'    => $this->post_id,
							),
						),
					),
					'menu_order'            => $this->menu_order,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => 1,
					'description'           => '',
				)
			);
		endif;
	}

	public function block_preview() {}

	public function output() {}
}

