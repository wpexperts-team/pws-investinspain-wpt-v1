<?php
get_header(); ?>
<?php get_template_part( 'template-parts/post-thumbnail-image' ); ?>
<div class="o-row o-row--padding-default">
	<div class="o-row__container">
		<?php if ( have_posts() ) : ?>
			<?php
			while ( have_posts() ) :
				the_post();
				?>
				<?php // IDEA: Move this to a get_template_part() ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_title( '<h1 class="h1 h1--line">', '</h1>' ); ?>
					<?php the_content(); ?>
				</div>
			<?php endwhile ?>
			<?php else : ?>
			<!-- Do Nothing -->
		<?php endif; ?>
	</div>
</div>

<?php
get_footer();
