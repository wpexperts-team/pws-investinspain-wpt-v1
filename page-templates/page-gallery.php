<?php /* Template Name: Gallery */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/post-thumbnail-image' ); ?>
<div class="o-row o-row--padding-default">
	<div class="o-row__container">
		<?php
		the_title( '<h1 class="h1 h1--line">', '</h1>' );
		the_content();
		?>
	</div>
</div>

<?php
$name   = 'cb_gallery1_gallery';
$images = get_field( $name, $post->ID );
$size   = 'medium_square';
if ( $images ) :
	?>
	<div class="o-row o-background-color-2 o-background-color-2--light">
		<div class="o-row__container">
			<div class="o-layout-7">
				<?php
				foreach ( $images as $image ) :
					?>
					<div class="o-layout-7__item">
						<a href="<?php echo $image['sizes']['xl']; ?>">
							<div class="o-image">
								<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>
