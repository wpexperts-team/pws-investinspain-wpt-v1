<?php /* Template Name: Location */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/post-thumbnail-image' ); ?>

<div class="o-row o-row--padding-default">
	<div class="o-row__container">
		<?php
		the_title( '<h1 class="h1 h1--line">', '</h1>' );
		the_content();
		?>
	</div>
</div>

<?php
$name  = 'cb_location1';
$image = get_field( $name . '_image', $post->ID );
$size  = 'xxl';
if ( $image ) {
	?>
	<div class="o-row">
		<div class="o-layout-1 o-layout-1--image">
			<div class="o-background-image">
				<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
			</div>
		</div>
	</div>
	<?php
}
?>

<?php get_footer(); ?>
