<?php /* Template Name: Properties */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/post-thumbnail-image' ); ?>

<div class="o-row o-layout-6">
	<div class="o-row__container o-layout-6__container">
		<div class="o-content">
			<?php the_title( '<h1 class="h1 h1--line">', '</h1>' ); ?>
			<?php the_content(); ?>
			<?php
			// Check if there are property categories
			$terms = get_terms(
				array(
					'taxonomy'   => 'property-category',
					'hide_empty' => false,
				)
			);

			// If property category list them per category
			if ( $terms ) {
				foreach ( $terms as $term ) {
					if ( $term->count != 0 ) {
						$args       = array(
							'post_type'      => 'property',
							'orderby'        => 'menu_order',
							'order'          => 'ASC',
							'post_status'    => 'publish',
							'posts_per_page' => -1,
							'tax_query'      => array(
								array(
									'taxonomy' => 'property-category',
									'field'    => 'id',
									'terms'    => $term->term_id,
								),
							),
						);
						$properties = get_posts( $args );
						echo '<h3 class="u-color-2">' . $term->name . '</h3>';
						include( get_template_directory() . '/includes/property-row.php' );
					}
				}
			}

			?>
			<?php
			$args        = array(
				'post_type'      => 'property',
				'orderby'        => 'menu_order',
				'order'          => 'ASC',
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'tax_query'      => array(
					array(
						'taxonomy' => 'property-category',
						'terms'    => get_terms( 'property-category', [ 'fields' => 'ids' ] ),
						'operator' => 'NOT IN',
					),
				),
			);
			$properties  = get_posts( $args );
			$count_terms = 0;
			foreach ( $terms as $term ) {
				$count_terms = $count_terms + $term->count;
			}
			if ( $count_terms != 0 ) {
				echo '<h3 class="u-color-2">' . __( 'Default', 'pws-investinspain-wpt-v1' ) . '</h3>';
			}
			include( get_template_directory() . '/includes/property-row.php' );
			?>
		</div>
	</div>
</div>

<?php get_template_part( 'template-parts/project-files' ); ?>

<?php get_footer(); ?>
