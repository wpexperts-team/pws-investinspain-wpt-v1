<?php /* Template Name: Project */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/post-thumbnail-image' ); ?>
<?php
$data = get_post_meta( $post->ID, '_pws_data_1', true );
?>
<div class="o-row o-row--padding-default">
	<div class="o-row__container">
		<?php
		the_title( '<h1 class="h1 h1--line">', '</h1>' );
		the_content();
		?>
	</div>
</div>

<?php
$name    = 'cb_project1' . '_';
$title   = $name . 'title';
$content = $name . 'content';
if ( get_field( $title, $post->ID ) && get_field( $content, $post->ID ) ) {
	?>
	<div class="o-row o-row--padding-default o-background-color-1 o-background-color-1--extra-light">
		<div class="o-row__container">
			<h2 class="h2 h2--line"><?php echo get_field( $title, $post->ID ); ?></h2>
			<?php echo get_field( $content, $post->ID ); ?>
		</div>
	</div>
<?php } ?>

<?php
$name    = 'cb_project2' . '_';
$image   = $name . 'image';
$title   = $name . 'title';
$content = $name . 'content';
?>
<div class="o-row o-row--padding-default-bottom">
	<div class="o-layout-2">
		<div class="o-layout-2__content">
			<div class="o-content o-content--padding o-content--color-white o-content--max-width o-content--right o-background-color-1">
				<h2 class="h2 h2--line"><?php echo get_field( $title, $post->ID ); ?></h2>
				<?php echo get_field( $content, $post->ID ); ?>
			</div>
		</div>
		<div class="o-layout-2__image">
			<div class="o-background-image o-top">
				<?php $field = get_field( $image, $post->ID ); ?>
				<img src="<?php echo $field['sizes']['xl']; ?>">
			</div>
		</div>
	</div>
</div>


<?php
$name   = 'cb_project4_gallery';
$images = get_field( $name, $post->ID );
$size   = 'xxl';
if ( $images ) :
	?>
	<div class="o-row">
		<div class="o-layout-3">
			<?php
			foreach ( $images as $image ) :
				?>
				<div class="o-layout-3__item">
					<div class="o-background-image">
						<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>
