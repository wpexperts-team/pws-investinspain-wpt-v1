<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

<div class="o-row">
	
	<div class="o-layout-1">
		<?php
		$youtube = get_option( 'options_pws_project_settings_project_setting_video_youtube_id' );
		$vimeo   = get_option( 'options_pws_project_settings_project_setting_video_vimeo_id' );
		if ( $youtube || $vimeo ) {
			Pws_Investinspain\Project::video_output();
		} else {
			$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
			$img               = wp_get_attachment_image_src( $post_thumbnail_id, 'xxl' );
			echo '<img src="' . $img[0] . '" alt="">';
		}
		?>
		
	</div>
</div>

<?php
$field    = 'cb_home1_list_item';
$icon     = $field . '_icon';
$title    = $field . '_title';
$subtitle = $field . '_subtitle';
if ( have_rows( $field, $post->ID ) ) :
	?>
	<div class="o-row o-row--padding-default o-background-color-2 o-background-color-2--extra-light">
		<div class="o-row__container">
			<div class="o-layout-4">
				<?php
				while ( have_rows( $field, $post->ID ) ) :
					the_row();
					?>
					<div class="o-layout-4__item">
						<div class="o-layout-4__image">
							<img src="<?php echo get_sub_field( $icon ); ?>">
						</div>
						<div class="o-layout-4__content">
							<h2 class="o-layout-4__title"><?php echo get_sub_field( $title ); ?></h2>
							<h4 class="o-layout-4__subtitle"><?php echo get_sub_field( $subtitle ); ?></h4>
						</div>
						<div class="o-layout-4__action">
							<!-- <a href="" class="o-button">Meer info</a> -->
						</div>
					</div>
					<?php
				endwhile;
				?>
			</div>
		</div>
	</div>
	<?php
else :
	// no rows found
endif;
?>

<?php
$name    = 'cb_home2' . '_';
$image   = $name . 'image';
$title   = $name . 'title';
$content = $name . 'content';
?>
<div class="o-row">
	<div class="o-layout-2 o-layout-2--reverse">
		<div class="o-layout-2__image">
			<div class="o-background-image o-top">
				<?php $field = get_field( $image, $post->ID ); ?>
				<img src="<?php echo $field['sizes']['xl']; ?>">
			</div>
		</div>
		<div class="o-layout-2__content">
			<div class="o-content o-content--padding">
				<h2 class="h2 h2--line"><?php echo get_field( $title, $post->ID ); ?></h2>
				<?php echo get_field( $content, $post->ID ); ?>
			</div>
		</div>
	</div>
</div>

<?php
$name    = 'cb_home3' . '_';
$image   = $name . 'image';
$title   = $name . 'title';
$content = $name . 'content';
?>
<div class="o-layout-5">
	<div class="o-layout-5__content">
		<div class="o-content o-content--padding">
			<h2><?php echo get_field( $title, $post->ID ); ?></h2>
			<?php echo get_field( $content, $post->ID ); ?>
		</div>
	</div>
	<div class="o-layout-5__image">
		<div class="o-background-image">
			<?php $field = get_field( $image, $post->ID ); ?>
			<img src="<?php echo $field['sizes']['xxl']; ?>">
		</div>
	</div>
</div>

<?php get_footer(); ?>
