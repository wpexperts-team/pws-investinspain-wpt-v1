
<footer>
	<a name="contact"></a>
	<?php
	$form = get_option( 'options_pws_site_setting_contact_form' );
	if ( $form ) { ?>
		<div class="o-row c-contact">
			<div class="o-row__container c-contact__container">
				<div class="c-contact__form">
					<div class="o-content">
						<?php
						gravity_form(
							$id                  = $form,
							$display_title       = true,
							$display_description = true,
							$display_inactive    = false,
							$field_values        = null,
							$ajax                = true,
							$tabindex            = 10,
							$echo                = true
						);
						?>
					</div>
				</div>
				<div class="c-contact__info">
					<div class="o-content">
						<?php
						$form_brochure = get_option( 'options_pws_project_settings_brochure_form' );
						if ( $form_brochure ) {
							gravity_form(
								$id                  = $form_brochure,
								$display_title       = true,
								$display_description = true,
								$display_inactive    = false,
								$field_values        = null,
								$ajax                = true,
								$tabindex            = 20,
								$echo                = true
							);
						}
						?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<section class="c-footer-1">
		<div class="c-footer-1__container">
			<div class="c-footer-1__col1">
				<?php
				// Primary Menu
				$wp_nav_menu = array(
					'theme_location'  => 'primary',
					'menu'            => '',
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => '',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="c-footer-nav-menu-1">%3$s</ul>',
					'depth'           => 0,
					'walker'          => '',
				);
				wp_nav_menu( $wp_nav_menu );
				?>
			</div>
			<div class="c-footer-1__col2">
				<?php
				// Primary Menu
				$wp_nav_menu = array(
					'theme_location'  => 'legal',
					'menu'            => '',
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => '',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="c-footer-nav-menu-2">%3$s</ul>',
					'depth'           => 0,
					'walker'          => '',
				);
				wp_nav_menu( $wp_nav_menu );
				?>
			</div>
		</div>
	</section>
</footer>
