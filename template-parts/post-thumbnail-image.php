<div class="o-row o-layout-1 o-layout-1--image">
	<div class="o-background-image">
		<?php
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		$img               = wp_get_attachment_image_src( $post_thumbnail_id, 'xxl' );
		?>
		<img src="<?php echo $img[0]; ?>" alt="">
	</div>
</div>
