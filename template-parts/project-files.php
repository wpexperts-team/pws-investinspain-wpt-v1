<?php
$files = get_field( 'pws_project_settings_project_files', 'option' );
if ( $files ) { ?>
	<div class="o-row o-row--padding-default-bottom o-background-color-2 o-background-color-2--light">
		<div class="o-layout-2 o-layout-2--reverse">
			<div class="o-layout-2__content">
				<div class="o-content o-content--padding">
					<h2 class="h2 h2--line"><?php _e( 'Project Documents', 'pws-investinspain' ); ?></h2>
					<?php
					if ( have_rows( 'pws_project_settings_project_files', 'option' ) ) :
						?>
						<ul class="c-project-files__list">
							<?php
							while ( have_rows( 'pws_project_settings_project_files', 'option' ) ) :
								the_row();
								$field = get_sub_field( 'pws_project_settings_file', 'option' );
								?>
								<li class="c-project-files__item">
									<div class="c-project-files__item-image"></div>
									<div class="c-project-files__item-content">
										<h4 class="c-project-files__item-title"><?php echo $field['title']; ?></h4>
										<?php echo $field['description']; ?>
									</div>
									<div class="c-project-files__item-actions">
										<a href="<?php echo $field['url']; ?>" target="_blank" class="o-button o-button--color-1"><?php _e( 'View', 'pws-investinspain' ); ?></a>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
						<?php
					else :
						// no rows found
					endif;
					?>
				</div>
			</div>
			<div class="o-layout-2__image">
				<div class="o-background-image o-top">
					<?php
					$post_thumbnail_id = get_post_thumbnail_id( Theme_Admin::get_page_template_id_by_name( 'page-project' ) );
					$img               = wp_get_attachment_image_src( $post_thumbnail_id, 'xxl' );
					?>
					<img src="<?php echo $img[0]; ?>" alt="">
				</div>
			</div>
		</div>
	</div>
<?php } ?>
