<div class="c-mobile-nav-icon">
  <i class="fas fa-bars"></i>
</div>
<div class="c-mobile-nav-content">
	<div class="c-mobile-nav-content__header"></div>
	<div class="c-mobile-nav-content__body">
		<nav>
			<?php
			// Primary Menu
			$wp_nav_menu = array(
				'theme_location'  => 'primary',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => '',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="c-mobile-nav-menu-main">%3$s</ul>',
				'depth'           => 0,
				'walker'          => '',
			);
			wp_nav_menu( $wp_nav_menu );
			?>
		</nav>
	</div>
	<div class="c-mobile-nav-content__footer">
	</div>
</div>

<script type="text/javascript">
jQuery(function($) {
  $(".c-mobile-nav-icon").click(function(){
	$(".c-mobile-nav-content").slideToggle();
	$(".c-mobile-nav-content").css("display","flex");
	$(".c-mobile-nav-icon").toggleClass("c-mobile-nav-icon--active");
  });
});
</script>

<header>
	<div class="c-header">
		<div class="c-header__wrapper">
			<div class="c-header__logo">
				<a href="<?php echo get_home_url(); ?>">
					<?php
					$image_id = get_option( 'options_pws_site_setting_logo' );
					if ( $image_id ) {
						$img = wp_get_attachment_image_src( $image_id, 'large' );
						echo '<img src="' . $img[0] . '" alt="">';
					}
					?>
				</a>
			</div>
			<div class="c-header__navigation">
				<nav>
					<?php
					// Primary Menu
					$wp_nav_menu = array(
						'theme_location'  => 'primary',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => '',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="c-header-nav-primary">%3$s</ul>',
						'depth'           => 0,
						'walker'          => '',
					);
					wp_nav_menu( $wp_nav_menu );
					?>
				</nav>
			</div>
		</div>
	</div>
</header>
